@extends('layouts.app')

@section('content')

  <table class="table table-striped table-bordered" >
    <thead>
    <tr>
     <th>Name</th>
     <th>Airline</th>
     <th>View Information</th>
    </tr>
     
    </thead>

    <tbody>
     @foreach($flights as $flight) 
    
       <tr>
         <td> {{ $flight->name }} </td>
         <td> {{ $flight->airline }}</td>
         <td> <a href="{{ url('flights/'.$flight->id ) }}" class="btn btn-primary">View INfo</a>   </td>
       </tr>

     @endforeach

    </tbody>

  </table>

@endsection