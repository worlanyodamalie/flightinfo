@extends('layouts.app')

@section('content')

 <div class="row">
 <div class="col-md-10 col-md-offset-1">
   @include('includes.flash')
    <form  class="form-horizontal"  method="POST" action="{{ url('/flights')}}">
    
      {!! csrf_field() !!}

       <div class="form-group">
         <div class="col-md-6">
         <label for="flight_name" class="col-md-4 control-label">Flight Name</label>
         </div>

         <div class="col-md-6">
         <input id="name" type="text" class="form-control" name="name" >
          @if($errors->has('name'))
          <span class="help-block">
            <strong> {{ $errors->first('name')}} </strong>
          </span>
          @endif
         </div>
       </div>  

        <div class="form-group">
        <div class="col-md-6">
         <label for="airline" class="col-md-4 control-label">AirLine</label>    
         </div>

         <div class="col-md-6">
         <input id="airline" type="text" class="form-control" name="airline" >
         </div>


        </div>   

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-ticket"></i> Submit
                </button>
            </div>
        </div>

    </form>

 </div>
 </div>

@endsection