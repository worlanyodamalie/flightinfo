<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected $fillable = ['name','airline','user_id'];
}
