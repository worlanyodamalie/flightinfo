<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Flight;
use App\User;
use App\Http\Controllers\Controller;


class FlightController extends Controller
{
     /**
     * Create a new flight instance.
     *
     * @param  Request  $request
     * @return Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }
    
     public function index()
     {  
        $flights = Auth::user()->flights()->get()->all();
        
        return view('flights.index',compact('flights'));     
     }

     public function create()
      {
          return view('flights.create');
      }

     public function store(Request $request)
     {
         $this->validate($request,
        [
            'name' => 'required',
            'airline' => 'required'
        ]);
        
        $flight = new Flight(
            [
                'name' => $request->input('name'),
                'airline' => $request->input('airline'),
                'user_id' => Auth::user()->id    
            ]

        ); 

        $flight->save();

        return redirect('/flights');

        //  $flight = new Flight;
        //  $flight->name = $request->name;
        //  //$flight->user_id = $request->user->id
        //  $flight->save();
     }

     public function show($id)
     {
          $flight = Flight::where('id',$id)->firstOrFail();

          return view('flights.show', compact('flight'));
     }



}
